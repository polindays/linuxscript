#!/bin/bash
NOW=$(date +"%m-%d-%Y")
FILE="battery.$NOW.txt"
if [ -d ./BatteryStatus ]; then
	upower -i /org/freedesktop/UPower/devices/battery_BAT0 >> ./BatteryStatus/$FILE
else 
	mkdir BatteryStatus
	upower -i /org/freedesktop/UPower/devices/battery_BAT0 >> ./BatteryStatus/$FILE
fi
echo $NOW
echo $(grep -i -m 1 "capacity" ./BatteryStatus/$FILE)
#read -p "Press enter to continue"
sleep 3s
